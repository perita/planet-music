/*
Planet Music — Entry for Itch.io’s Mini Jam 107: Music²
Copyright (C) 2022  jordi fita mas <jfita@peritasoft.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import hxsl.Types.Vec;

class Sun {
	public final pos:h2d.col.Point;
	public var mass:Float;

	public function new(x, y:Float, mass:Float) {
		pos = new h2d.col.Point(x, y);
		this.mass = mass;
	}

	public function draw(g:h2d.Graphics) {
		g.drawCircle(pos.x, pos.y, mass);
	}
}
