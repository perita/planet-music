/*
Planet Music — Entry for Itch.io’s Mini Jam 107: Music²
Copyright (C) 2022  jordi fita mas <jfita@peritasoft.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

enum abstract EnvelopStage(Int) from Int to Int {
	var Attack;
	var Decay;
	var Sustain;
	var Release;
}

class Waveform extends hxd.snd.Data {
	var totalDuration(get, null):Float;

	public function new(note:Int) {
		samplingRate = 44100; // Hz
		channels = 1;
		sampleFormat = I16;
		samples = Math.floor(samplingRate * totalDuration);
		rawData = haxe.io.Bytes.alloc(samples * getBytesPerSample());
		generateWaveform(note);
	}

	function get_totalDuration():Float {
		return envelope[0] + envelope[1] + envelope[2] + envelope[3];
	}

	override function decodeBuffer(out:haxe.io.Bytes, outPos:Int, sampleStart:Int, sampleCount:Int) {
		var bpp = getBytesPerSample();
		out.blit(outPos, rawData, sampleStart * bpp, sampleCount * bpp);
	}

	function generateWaveform(note:Int) {
		final chromaticRatio = 1.059463094359295264562; // 12 notes per octave; notes separated by factor 2^{1/12}
		final pitch = Math.pow(chromaticRatio, note - 69.0) * 440; // Given MIDI note m, freq(m) = 440 × 2^(m-69) / 12 | m ∈ [0, 127], f(60) = 261.6256 Hz
		final phaseIncrement = pitch / samplingRate;
		var pos = 0;
		final posStride = hxd.snd.Data.formatBytes(sampleFormat);
		var phase = 0.0;
		var envStage = Attack;
		var envTime = 0.0;
		for (i in 0...samples) {
			envTime += 1 / samplingRate;
			if (envTime > envelope[envStage]) {
				if (envStage == Release) {
					continue;
				}
				envTime -= envelope[envStage];
				envStage = envStage + 1;
			}
			final envVol = switch (envStage) {
				case Attack: envTime / envelope[Attack];
				case Decay: sustainLevel + (1.0 - sustainLevel) * Math.pow(1.0 - envTime / envelope[Decay], 3.0);
				case Sustain: sustainLevel;
				case Release: sustainLevel * Math.pow(1.0 - envTime / envelope[Release], 3.0);
			}
			phase += phaseIncrement;
			final sample = Math.sin(phase * Math.PI * 2);
			rawData.setUInt16(pos, Std.int(sample * envVol * 0.5 * 32767));
			pos += posStride;
		}
	}

	private static function getEnvelopeAmpByNode(base_node:Int, envelope_data:Array<Float>, cursor:Float):Float {
		final n1:Float = base_node;
		final n2:Float = base_node + 1;
		final relative_cursor_pos = (cursor - n1) / (n2 - n1);
		final amp_diff = (envelope_data[base_node + 1] - envelope_data[base_node]);
		return envelope_data[base_node] * (relative_cursor_pos * amp_diff);
	}

	final rawData:haxe.io.Bytes;
	final envelope = [0.005, 0.1, 0.125, 1];
	final sustainLevel = 0.3;
}
