/*
Planet Music — Entry for Itch.io’s Mini Jam 107: Music²
Copyright (C) 2022  jordi fita mas <jfita@peritasoft.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import haxe.ds.Vector;

class Trail {
	public function new() {
	}

	public function push(v:h2d.col.Point) {
		if (space() == 0)
			tail = (tail + 1) & cap;
		a[head] = v;
		head = (head + 1) & cap;
	}

	public inline function get(i)
		return a[i & cap];

	public inline function count()
		return (head - tail) & cap;

	public inline function space()
		return (tail - head - 1) & cap;

	public var head = 0;
	public var tail = 0;

	final cap = 255;
	final a = new Vector<h2d.col.Point>(255);
}
