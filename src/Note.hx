/*
Planet Music — Entry for Itch.io’s Mini Jam 107: Music²
Copyright (C) 2022  jordi fita mas <jfita@peritasoft.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import hxd.fs.BytesFileSystem.BytesFileEntry;

class Note extends hxd.res.Sound {
	public function new(note:String, waveform:Waveform) {
		super(new BytesFileEntry(note, null));
		this.waveform = waveform;
	}

	override function getData():hxd.snd.Data {
		return waveform;
	}

	var waveform:Waveform;
}
