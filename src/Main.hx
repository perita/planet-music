/*
Planet Music — Entry for Itch.io’s Mini Jam 107: Music²
Copyright (C) 2022  jordi fita mas <jfita@peritasoft.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import hxd.Key;
import hxd.Window;

class Main extends hxd.App {
	public static function main() {
		#if js
		hxd.Res.initEmbed();
		#else
		hxd.Res.initLocal();
		#end
		new Main();
	}

	override function init() {
		engine.backgroundColor = 0xff2d2341;
		s2d.scaleMode = Fixed(960, 600, 1);
		g = new h2d.Graphics(s2d);

		suns.push(new Sun(960 / 2, 600 / 2, 92));
		suns.push(new Sun(3 * 960 / 4, 600 / 4, 94));

		planets.push(new Planet(960 / 2, 600 / 4, 250, 0));
		planets.push(new Planet(960 / 2, 3 * 600 / 4, -300, -125));

		notes = [
			for (octave in 3...6)
				for (note in [0, 2, 5, 7, 9])
					new Note(Std.string('${octave}_${note}'), new Waveform((octave + 1) * 12 + note))
		];
		notes.push(new Note('6_0', new Waveform(84)));
		notes.reverse();
		noteHeight = (maxMarkerHeight / notes.length);

		function updateMarker() {
			final stage = Window.getInstance();
			marker.x = (600 - stage.height / scale) / 2;
			marker.y = stage.height / scale;
		}

		Window.getInstance().addResizeEvent(updateMarker);

		Window.getInstance().addEventTarget((e) -> {
			switch (e.kind) {
				case EPush:
					switch (e.button) {
						case 0: placePlanet();
						case 1: placeSun();
						case 2: removeSun();
					}
				case EWheel:
					switch (s2d.scaleMode) {
						case Fixed(w, h, zoom):
							scale = Math.min(1.75, Math.max(0.25, zoom - e.wheelDelta * 0.05));
							s2d.scaleMode = Fixed(w, h, scale);
							updateMarker();
						case _:
					}
				case EKeyDown:
					if (e.keyCode == Key.R) {
						suns.resize(0);
						planets.resize(0);
					}
				case _:
			}
		});
	}

	override function update(dt:Float) {
		if (!paused) {
			time += dt;
			while (time > step) {
				time -= time;
				ripples.update(step);
				for (planet in planets) {
					if (planet.update(step, suns)) {
						final y = planet.y;
						ripples.alloc(planet.color, 960 / 2, y);
						final note = Math.floor(Math.max(0, Math.min(notes.length - 1, (y + markerOffset) / noteHeight)));
						notes[note].play();
					}
				}
			}
		}
		redraw();
	}

	function redraw() {
		g.clear();
		g.beginFill(0xffddc782);
		for (sun in suns) {
			sun.draw(g);
		}
		if (newSun != null) {
			newSun.draw(g);
		}
		for (planet in planets) {
			planet.draw(g);
		}
		if (newPlanet != null) {
			newPlanet.draw(g);
		}
		g.setColor(0xd9d9d9);
		for (y in 0...notes.length) {
			g.drawRect(960 / 2 - 1.5 / scale, -markerOffset + y * noteHeight + 3 / scale, 3 / scale, noteHeight - 3 / scale);
		}
		ripples.draw(g);
		g.endFill();
		for (planet in planets) {
			planet.drawTrail(g, scale);
		}
		if (newPlanet != null) {
			newPlanet.drawPreview(g, scale);
		}
	}

	function placeSun() {
		newSun = new Sun(s2d.mouseX, s2d.mouseY, 10);
		startCapture(e -> switch (e.kind) {
			case ERelease:
				if (e.button == 1) {
					suns.push(newSun);
					stopCapture();
				}
			case EMove:
				final dx = newSun.pos.x - s2d.mouseX;
				final dy = newSun.pos.y - s2d.mouseY;
				newSun.mass = Math.max(10.0, Math.sqrt(dx * dx + dy * dy));
			case _:
		});
	}

	function removeSun() {
		final pos = new h2d.col.Point(s2d.mouseX, s2d.mouseY);
		var i = 0;
		while (i < suns.length) {
			final sun = suns[i];
			if (sun.pos.distance(pos) <= sun.mass) {
				suns.splice(i, 1);
			} else {
				i++;
			}
		}
	}

	function placePlanet() {
		newPlanet = new PlanetLauncher(s2d.mouseX, s2d.mouseY);
		startCapture(e -> switch (e.kind) {
			case ERelease:
				if (e.button == 0) {
					planets.push(newPlanet.get());
					stopCapture();
				}
			case EMove:
				newPlanet.pullBack(s2d.mouseX, s2d.mouseY, step, suns);
			case _:
		});
	}

	function startCapture(onEvent:hxd.Event->Void) {
		paused = true;
		s2d.startCapture(e -> switch (e.kind) {
			case EKeyDown:
				if (e.keyCode == Key.ESCAPE) {
					stopCapture();
				}
			case _: onEvent(e);
		});
	}

	function stopCapture() {
		s2d.stopCapture();
		newSun = null;
		newPlanet = null;
		paused = false;
	}

	final planets = new Array<Planet>();
	final suns = new Array<Sun>();
	final maxMarkerHeight = 2400;
	final markerOffset = 900;
	var noteHeight:Float;
	var g:h2d.Graphics;
	var notes:Array<Note>;
	var time = 0.0;
	final step = 1 / 60;
	final marker = new h2d.col.Point(0, 600);
	final ripples = new Ripples(256);
	var scale = 1.0;
	var newSun:Sun;
	var paused = false;
	var newPlanet:PlanetLauncher;
}
