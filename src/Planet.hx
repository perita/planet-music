/*
Planet Music — Entry for Itch.io’s Mini Jam 107: Music²
Copyright (C) 2022  jordi fita mas <jfita@peritasoft.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class Planet {
	public var y(get, null):Float;

	public function new(x, y:Float, vx, vy:Float, color:Null<Int> = null) {
		position = new h2d.col.Point(x, y);
		velocity = new h2d.col.Point(vx, vy);
		this.color = color == null ? randomColor() : color;
	}

	public static function randomColor():Int
		return colors[Math.floor(Math.random() * colors.length)];

	private function get_y():Float {
		return position.y;
	}

	public function update(dt:Float, suns:Array<Sun>):Bool {
		final acceleration = new h2d.col.Point(0, 0);
		for (sun in suns) {
			final distance = sun.pos.sub(position);
			final lengthSq = distance.lengthSq();
			if (lengthSq < 0.001) {
				continue;
			}
			final force = G * mass * sun.mass / distance.lengthSq();
			distance.normalize();
			acceleration.x += distance.x * force;
			acceleration.y += distance.y * force;
		}
		velocity.x += acceleration.x * dt * boost;
		velocity.y += acceleration.y * dt * boost;

		final lastPosition = position;
		position = new h2d.col.Point(position.x + velocity.x * dt * boost, position.y + velocity.y * dt * boost);
		prevPositions.push(lastPosition);
		return (lastPosition.x < 960 / 2 && position.x > 960 / 2) || (lastPosition.x > 960 / 2 && position.x < 960 / 2);
	}

	public function draw(g:h2d.Graphics) {
		g.setColor(color);
		g.drawCircle(position.x, position.y, mass);
	}

	public function drawTrail(g:h2d.Graphics, scale:Float, alphaFunc:(i:Int, length:Int) -> Float = null, drawLast:Bool = true) {
		final length = prevPositions.count();
		if (length < 2)
			return;
		final offset = prevPositions.tail;
		var prev = prevPositions.get(offset);
		if (alphaFunc == null) {
			alphaFunc = (i, length) -> i / length;
		}
		for (i in 1...length) {
			g.lineStyle(1 / scale, color, alphaFunc(i, length));
			g.moveTo(prev.x, prev.y);
			final cur = prevPositions.get(i + offset);
			g.lineTo(cur.x, cur.y);
			prev = cur;
		}
		if (drawLast) {
			g.lineTo(position.x, position.y);
		}
	}

	public function setPosition(x, y:Float) {
		position.x = x;
		position.y = y;
	}

	var position:h2d.col.Point;
	var prevPositions = new Trail();
	var velocity:h2d.col.Point;
	final mass = 10.0;

	public final color:Int;

	public static final colors = [
		0xe8979a,
		0x9fe897,
		0x97a7e8,
		0xb597e8,
		0xe8b397,
		0xffbd4a,
		0xf2421b,
		0x27ad1d,
		0x91a19f,
	];
	public static final G = 10000;
	public static final boost = 1.5;
}
