/*
Planet Music — Entry for Itch.io’s Mini Jam 107: Music²
Copyright (C) 2022  jordi fita mas <jfita@peritasoft.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class PlanetLauncher {
	public function new(x, y:Float) {
		this.x = x;
		this.y = y;
		color = Planet.randomColor();
	}

	public function get():Planet {
		return new Planet(x, y, vx, vy, color);
	}

	public function pullBack(dx:Float, dy:Float, dt:Float, suns:Array<Sun>) {
		vx = x - dx;
		vy = y - dy;
		preview = get();
		for (i in 0...255) {
			preview.update(dt, suns);
		}
		preview.setPosition(x, y);
	}

	public function draw(g:h2d.Graphics) {
		if (preview != null) {
			preview.draw(g);
		}
	}

	public function drawPreview(g:h2d.Graphics, scale:Float) {
		if (preview != null) {
			preview.drawTrail(g, scale, (i, length) -> 1.0 - i / length, false);
		}
	}

	final x:Float;
	final y:Float;
	var vx:Float;
	var vy:Float;
	final color:Int;
	var preview:Planet;
}
