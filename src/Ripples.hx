/*
Planet Music — Entry for Itch.io’s Mini Jam 107: Music²
Copyright (C) 2022  jordi fita mas <jfita@peritasoft.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import h2d.Graphics;
import hxd.impl.AllocPos;

class Ripples {
	var all:haxe.ds.Vector<Ripple>;
	var nalloc:Int;

	public var size(get, never):Int;

	inline function get_size()
		return all.length;

	public function new(count:Int) {
		all = new haxe.ds.Vector(count);
		nalloc = 0;

		for (i in 0...count) {
			var p = @:privateAccess new Ripple(this);
			all[i] = p;
			p.kill();
		}
	}

	public inline function alloc(color: Int, x:Float, y:Float, ?pos:AllocPos):Ripple {
		return if (nalloc < all.length) {
			// Use a killed part
			var p = all[nalloc];
			@:privateAccess p.reset(color, x, y);
			@:privateAccess p.poolIdx = nalloc;
			nalloc++;
			#if debug
			p.allocPos = pos;
			#end
			p;
		} else {
			// Find oldest active part
			var best:Ripple = null;
			for (p in all)
				if (best == null || @:privateAccess p.stamp <= @:privateAccess best.stamp) // TODO optimize that
					best = p;

			@:privateAccess best.onKillCallbacks();
			@:privateAccess best.reset(color, x, y);
			#if debug
			best.allocPos = pos;
			#end
			best;
		}
	}

	inline function free(kp:Ripple) {
		if (all != null) {
			if (nalloc > 1) {
				var idx = @:privateAccess kp.poolIdx;
				var tmp = all[idx];
				all[idx] = all[nalloc - 1];
				@:privateAccess all[idx].poolIdx = idx;
				all[nalloc - 1] = tmp;
				nalloc--;
			} else
				nalloc = 0;
		}
	}

	/** Count active particles **/
	public inline function count()
		return nalloc;

	/** Destroy every active particles **/
	public function clear() {
		// Because new particles might be allocated during onKill() callbacks,
		// it's sometimes necessary to repeat the clear() process multiple times.
		var repeat = false;
		var maxRepeats = 10;
		var p:Ripple = null;
		do {
			repeat = false;

			for (i in 0...size) {
				p = all[i];
				if (@:privateAccess p.onKillCallbacks())
					repeat = true;
				@:privateAccess p.reset();
				p.visible = false;
			}

			if (repeat && maxRepeats-- <= 0)
				throw("Infinite loop during clear: an onKill() callback is repeatingly allocating new particles.");
		} while (repeat);
	}

	public inline function killAllWithFade() {
		for (i in 0...nalloc) {
			var p = all[i];
			p.lifeS = 0;
		}
	}

	public function dispose() {
		all = null;
	}

	public inline function update(dt:Float, ?updateCb:Ripple->Void) {
		var i = 0;
		while (i < nalloc) {
			final p = all[i];
			@:privateAccess p.updatePart(dt);
			if (!p.killed) {
				if (updateCb != null)
					updateCb(p);
				i++;
			}
		}
	}

	public inline function draw(g:Graphics) {
		for (i in 0...nalloc) {
			final p = all[i];
			if (p.visible) {
				p.drawPart(g);
			}
		}
	}
}

class Ripple {
	final pool:Ripples;

	public var poolIdx(default, null):Int;

	var stamp:Float;

	public var x:Float = 0;
	public var y:Float = 0;
	public var scale:Float = 1;
	public var alpha:Float = 1;
	public var visible:Bool = true;
	public var killed:Bool;
	public var lifeS:Float;
	public var color:Int;

	#if debug
	public var allocPos:AllocPos;
	#end

	private function new(p:Ripples, color: Int = 0xffffff, x:Float = 0, y:Float = 0) {
		pool = p;
		poolIdx = -1;
		reset(color, x, y);
	}

	public inline function updatePart(dt:Float) {
		lifeS -= dt;
		if (lifeS <= 0) {
			kill();
		} else {
			alpha -= 2 * dt;
			scale += 5 * dt;
		}
	}

	public inline function drawPart(g:h2d.Graphics) {
		g.setColor(color, alpha);
		g.drawCircle(x, y, 10 * scale);
	}

	inline function reset(color: Int = 0xffffff, x:Float = 0, y:Float = 0) {
		setPosition(x, y);
		this.color = color;
		scale = 1;
		alpha = 1;
		visible = true;
		killed = false;
		lifeS = 1;
		stamp = haxe.Timer.stamp();
	}

	public inline function setPosition(x, y:Float) {
		this.x = x;
		this.y = y;
	}

	public inline function kill() {
		if (!killed) {
			onKillCallbacks();

			alpha = 0;
			lifeS = 0;
			killed = true;
			visible = false;

			@:privateAccess pool.free(this);
		}
	}

	inline function onKillCallbacks() {
		var any = false;
		return any;
	}
}
